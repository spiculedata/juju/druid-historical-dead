import os
import stat
from charms.reactive import when, when_not, set_flag, context
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import resource_get, status_set
from charms.reactive.helpers import data_changed
from subprocess import check_call, Popen

@when_not('druid-historical.installed')
def install_druid_historical():
    archive = resource_get("druid")
    os.mkdir('/opt/druid_historical')
    cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_historical', '--strip', '1']
    check_call(cmd)

    archive = resource_get("mysql-extension")
    cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_historical/extensions']
    check_call(cmd)
    set_flag('druid-historical.installed')

@when('druid-historical.installed', 'zookeeper.ready', 'mysql.available', 'endpoint.config.connected')
@when_not('druid-historical.configured')
def configure_druid_historical():
    status_set('maintenance', 'Configuring Historical')

    # Common config being written here - later change to extract from Config charm

    config = endpoint_from_flag('endpoint.config.connected')
    config_string = config.get_config()
    config_file = open('/opt/druid_historical/conf/druid/_common/common.runtimes.properties', 'w')
    config_file.write(config_string)
    config_file.close()

    render('run_historical.sh', '/opt/druid_historical/run_historical.sh')
    st = os.stat('/opt/druid_historical/run_historical.sh')
    os.chmod('/opt/druid_historical/run_historical.sh', st.st_mode | stat.S_IEXEC)
    set_flag('druid-historical.configured')


@when('druid-historical.configured')
@when('java.ready')
def run_druid_historical(java):
    status_set('maintenance', 'Starting Historical...')
    cmd = ['/opt/druid_historical/run_historical.sh']
    check_call(cmd)
    set_flag('druid-historical.running')
    status_set('active', 'Historical running')


@when('zookeeper.joined')
@when_not('zookeeper.ready')
def wait_for_zookeeper(zookeeper):
    """
         We always run in Distributed mode, so wait for Zookeeper to become available.
    """
    status_set('waiting', 'Waiting for Zookeeper to become available')


@when_not('zookeeper.joined')
def wait_for_zkjoin():
    """
        Wait for Zookeeper
    """
    status_set('waiting', 'Waiting for Zookeeper to become joined')


@when_not('mysql.available')
def wait_for_mysqljoin():
    """
        Wait for MySQL
    """
    status_set('waiting', 'Waiting for MySQL to become joined')


def configure_zookeepers(zookeeper):
    """
        Check for new Zookeeper nodes, add them to list of zookeepers.

    :param zookeeper:
    :return:
    """
    zks = zookeeper.zookeepers()
    if data_changed('available.zookeepers', zks):
        zklist = ''
        for zk_unit in zookeeper.zookeepers():
            zklist += add_zookeeper(zk_unit['host'], zk_unit['port'])
        zklist = zklist[:-1]
        return zklist


def add_zookeeper(host, port):
    """
        Return a ZK hostline for the config.
    """
    return host+':'+port+','


def create_mysql_url(mysql):
    return 'jdbc:mysql://'+mysql.host()+':'+str(mysql.port())+'/'+mysql.database()
